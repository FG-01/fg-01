//$fn=20;
$fa=5;
$fs=1;
radius=1;
height_mul=4;
draw_supports=true;

module hex_diamond(x, y, z, height_mul, radius) {
    translate([x, y, z + 2*height_mul])
    rotate([-90, 0, 0])
    cylinder(r=radius, h=4*height_mul);
    
    translate([x, y + 2*height_mul, z])
    cylinder(r=radius, h=4*height_mul);
    
    translate([x - 2*height_mul, y + 2*height_mul, z + 2*height_mul])
    rotate([-90, 0, -90])
    cylinder(r=radius, h=4*height_mul);
}

module point_cyl(x, y, z, radius) {
    length = norm([x,y,z]);
    b = acos(z/length);
    c = atan2(y,x);

    rotate([0, b, c]) 
    cylinder(h=length, r=radius);
    
    //sphere(r=radius);

    translate([x, y, z])
    sphere(r=radius);
}

module hex_cross(x, y, z, height_mul, radius) {
    translate([x + height_mul, y + height_mul, z + height_mul])
    point_cyl(-2*height_mul, 2*height_mul, 2*height_mul, radius);
    
    translate([x + height_mul, y + 3*height_mul, z + height_mul])
    point_cyl(-2*height_mul, -2*height_mul, 2*height_mul, radius);
    
    translate([x - height_mul, y + height_mul, z + height_mul])
    point_cyl(2*height_mul, 2*height_mul, 2*height_mul, radius);
    
    translate([x - height_mul, y + 3*height_mul, z + height_mul])
    point_cyl(2*height_mul, -2*height_mul, 2*height_mul, radius);
}

module hex_support(x, y, z, height_mul, radius) {
    hex_cross(x, y, z, height_mul, radius);
}

module draw_lattice(n, rad, height, supports, fn) {
    end=n*2*height;
    for (i=[0:2*height:n*2*height]) {
        for (j=[0:2*height:n*2*height]) {
            for (l=[0:2*height:n*2*height]) {
                if (((i+j+l)/height)%4==0) {
                    hex_support(i, j, l, height, rad);
                } else {
                    if (supports) {
                        hex_diamond(i, j, l, height, rad);
                    }
                }
            }
        }
    }
}

module gen_lattice(n, rad, height, supports, fn) {
    radius=rad;
    height_mul=height;
    draw_supports=supports;
    $fn=fn;
    translate([height_mul*2, 0, 0])
    draw_lattice(n, rad, height, supports, fn);
}

module draw_box(n, height) {
    height_mul=height;
    translate([height_mul*2, height_mul*2, height_mul*2])
    cube(height_mul*2*(n));
}

/*#draw_box(4, 4);
gen_lattice(4, 1, 4, true, 30);*/
