# \[FG-01\]

![image](doc/scene.png "FG-01")

* Introduction
* [Scanning](doc/scanning.md)
* [Modeling](doc/modeling.md)
* [Printing](doc/printing.md)
* [Assembling](doc/assembling.md)
* [Testing](doc/testing.md)
* [Faq](doc/faq.md)

## Introduction

FG-01 is the first ever solution that helps you to defend gait recognition. The purpose of this project is to protect people from this kind of surveillance. Note that FG-01 makes you __pseudonymous__: it changes the way you walk, but it doesn't break the recognition process completely.

## Requirements

#### Software

* Meshroom (latest) or similar
* OpenSCAD 2019.1 or later
* Blender 2.8 or later
* Meshlab or similar (optional)
* Freecad (optional)

#### Hardware

* CUDA-compatible GPU (for Meshroom) [No GPU?](doc/faq.md#NVIDIA-GPU)
* FDM or SLA 3D-printer

#### Materials

* PLA, PETG or any other filament/plastic
* Rubber band (for boot mount)
* Weight

## Usage

FG-01 has many sections where you can install weight. If you change the center of gravity, the gait will also change.

### This particular tool helps to spoof gait NNs that detect step parameters (step length and foot tilt angle). Be aware that FG-01 changes gait to unique, but different identity. Use wisely.

You should also change clothes when you don't want to be seen. It's pretty much similar to Firefox containers: use different identities depending on the risks (low - regular usage, high - you're attending a protest). __Always wear different clothes while using FG-01, because gait NN can recognise you by your clothes.__ [Why?](doc/faq.md#CLOTHES)

Note that baggy clothes spoofs gait recognition the best and backpacks or other bags are also detected by the NN.

FG-01 won't guarantee the protection, because every gait detection algorithm works differently and their sources are unavailable. Do your own research. __Remember that without protection from the face recognition FG-01 will render itself useless__.
 
Spoiling your face will likely tie it to the current identity, so why changing identities is crucial.

### Never wear FG-01 regularly or for a long time, because this may impact your health. Changing your gait for long periods of time may lead to chronic pain or other issues. This is another reason to use it only in cases of urgency. [More](doc/faq.md#HEALTH)
