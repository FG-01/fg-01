import bpy
import os

out_model_path = "/path/to/out.stl"
shell_path = "/path/to/shell_out.stl"
boot_path = "/path/to/boot_out.stl"
lattice_chunk_path = "/path/to/lattice_chunk.stl"
segments_path = "/path/to/segments.stl"

segments_export_path = "/path/to/segments/"

render_segments = False

offset = 0.05
scale_factor = 2
lattice_count = 5

boolean_self = True
boolean_self_list = [False, False, True, False]

boolean_tbs = False
boolean_tbs_list = [False, True, False, False]

segments_self = True
segments_tbs = False

if render_segments:
    bpy.ops.import_mesh.stl(filepath=segments_path)
    bpy.ops.import_mesh.stl(filepath=boot_path)
    
    segments = bpy.context.scene.objects.get('Segments')
    boot = bpy.context.scene.objects.get('Boot Out')
    
    bpy.ops.object.select_all(action='DESELECT')
    segments.select_set(True)
    segm_mod = segments.modifiers.new('Segments', 'BOOLEAN')
    segm_mod.object = boot
    segm_mod.operation = 'DIFFERENCE'
    if segments_tbs:
        segm_mod.solver = 'FAST'
    if segments_self:
        segm_mod.use_self = True
    bpy.context.view_layer.objects.active = segments
    bpy.ops.object.modifier_apply(modifier='Segments')
    
    bpy.ops.object.select_all(action='DESELECT')
    segments.select_set(True)
    
    bpy.context.view_layer.objects.active = segments
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.separate(type='LOOSE')
    bpy.ops.object.mode_set(mode='OBJECT')
    
    if not os.path.isdir(segments_export_path):
        os.mkdir(segments_export_path)
    segm_list = list(s for s in bpy.data.objects if s.name.find("Segments") != -1)
    if segm_list is not None:
        for s in segm_list:
            bpy.ops.object.select_all(action='DESELECT')
            s.select_set(True)
            bpy.context.view_layer.objects.active = s
            bpy.ops.export_mesh.stl(filepath=os.path.join(segments_export_path, s.name + '.stl'),\
            use_selection=True, use_mesh_modifiers=True, use_scene_unit=True)
    
else:
    bpy.ops.import_mesh.stl(filepath=out_model_path)
    bpy.ops.import_mesh.stl(filepath=shell_path)
    bpy.ops.import_mesh.stl(filepath=boot_path)
    bpy.ops.import_mesh.stl(filepath=lattice_chunk_path)
    
    out = bpy.context.scene.objects.get('Out')
    shell = bpy.context.scene.objects.get('Shell Out')
    boot = bpy.context.scene.objects.get('Boot Out')
    chunk = bpy.context.scene.objects.get('Lattice Chunk')
    
    boot_shell = boot.copy()
    boot_shell.data = boot.data.copy()
    bpy.context.collection.objects.link(boot_shell)
    
    lattice_shell = out.copy()
    lattice_shell.data = out.data.copy()
    bpy.context.collection.objects.link(lattice_shell)
    
    bpy.ops.object.select_all(action='DESELECT')
    boot_shell.select_set(True)
    bpy.context.view_layer.objects.active = boot_shell
    #boot.select_set(False)
    #lattice_shell.select_set(False)
    
    bpy.ops.transform.resize(value=(1+offset, 1+offset, 1+offset))
    
    bpy.ops.object.select_all(action='DESELECT')
    #boot_shell.select_set(False)
    chunk.select_set(True)
    bpy.context.view_layer.objects.active = chunk
    
    bpy.ops.transform.resize(value=(scale_factor, scale_factor, scale_factor))
    
    # ------ First modifier ------
    bpy.ops.object.select_all(action='DESELECT')
    shell.select_set(True)
    shell_mod = shell.modifiers.new('Shell_Diff', 'BOOLEAN')
    shell_mod.object = boot_shell
    if boolean_tbs and boolean_tbs_list[0]:
        shell_mod.solver = 'FAST'
    if boolean_self and boolean_self_list[0]:
        shell_mod.use_self = True
    shell_mod.operation = 'DIFFERENCE'
    bpy.context.view_layer.objects.active = shell
    bpy.ops.object.modifier_apply(modifier='Shell_Diff')
    
    # ------ Second modifier ------
    bpy.ops.object.select_all(action='DESELECT')
    out.select_set(True)
    out_mod = out.modifiers.new('Diff', 'BOOLEAN')
    out_mod.object = shell
    if boolean_tbs and boolean_tbs_list[1]:
        out_mod.solver = 'FAST'
    if boolean_self and boolean_self_list[1]:
        out_mod.use_self = True
    out_mod.operation = 'DIFFERENCE'
    bpy.context.view_layer.objects.active = out
    bpy.ops.object.modifier_apply(modifier='Diff')
    
    # ------ Third modifier ------
    bpy.ops.object.select_all(action='DESELECT')
    out.select_set(True)
    out_boot_mod = out.modifiers.new('Out_Diff', 'BOOLEAN')
    out_boot_mod.object = boot
    if boolean_tbs and boolean_tbs_list[2]:
        out_boot_mod.solver = 'FAST'
    if boolean_self and boolean_self_list[2]:
        out_boot_mod.use_self = True
    out_boot_mod.operation = 'DIFFERENCE'
    bpy.context.view_layer.objects.active = out
    bpy.ops.object.modifier_apply(modifier='Out_Diff')
    
    # ------ Fourth modifier ------
    bpy.ops.object.select_all(action='DESELECT')
    lattice_shell.select_set(True)
    shell_lattice_mod = lattice_shell.modifiers.new('Lattice_Diff', 'BOOLEAN')
    shell_lattice_mod.object = shell
    if boolean_tbs and boolean_tbs_list[3]:
        shell_lattice_mod.solver = 'FAST'
    if boolean_self and boolean_self_list[3]:
        shell_lattice_mod.use_self = True
    shell_lattice_mod.operation = 'INTERSECT'
    bpy.context.view_layer.objects.active = lattice_shell
    bpy.ops.object.modifier_apply(modifier='Lattice_Diff')
    
    def arrayModf(x, y, z):
        array = chunk.modifiers.new('Chunk_Array', 'ARRAY')
        array.count = 5
        array.merge_threshold = 0.01
        array.relative_offset_displace = (x, y, z)
        array.use_merge_vertices_cap = True
        bpy.context.view_layer.objects.active = chunk
        #bpy.ops.object.modifier_apply(modifier='Chunk_Array')
    
    arrayModf(1.0, 0.0, 0.0)
    arrayModf(0.0, 1.0, 0.0)
    arrayModf(0.0, 0.0, 1.0)
    
    #bpy.ops.transform.resize(value=(scale_factor, scale_factor, scale_factor))
