# \[MODELING\]

## 1. OpenSCAD

Open the boot.scad file in OpenSCAD. You will need to change the variables at the top of the file. Please use this reference to set the parameters.

### 1.1 Model generation parameters

`angle`: Model rotation along the y axis. Default is 5.

`radius`: Model shape radius. Higher values mean wider model.

### 1.2 Model scale

`scale_x`: Model scale along the x axis.

`scale_y`: Model scale along the y axis.

`scale_z`: Model scale along the z axis.

You may want to change the y scaling (shrink the model) in order to make the model more comfortable to use.

### 1.3 Model scaling factor

`scale_factor`: Scale up/down the model to make it's size relative to the boot model size.

### 1.4 3D scan position

`translate_x`, `translate_y`, `translate_z`: Change the boot model position so that it's back part is placed exactly in the center of coordinates.

![image](modeling1.png "FG-01 modeling")

### 1.5 3D scan rotation

`scan_rotate_x`, `scan_rotate_y`, `scan_rotate_z`: Boot model rotation. The model should be perfectly aligned.

### 1.6 3D scan path

`scan_stl_path`: Path to the boot model stl.

### 1.7 Positioning

At this point you will need to test your config. Set the `mode` variable to `position` and press F5. When you're done, change it to `check_1` and press F5 again.

There should be enough space for the sections in the model, and the volume should be distributed equally (see the image).

![image](modeling2.png "FG-01 modeling")

### 1.8 Sections config

`sections_count`: Amount of sections, 360 should divide by `sections_count`.

`sections_size`: Each section's width.

`sections_angle`: Sections rotation. Should be equal to 0 or `angle`.

`start_angle`: Sections start angle \[1\].

`end_angle`: Sections end angle \[2\].

`sections_layers`: Amount of sections layers. 2 is optimal.

`layers_offset_z`: Bottom offset of the layers.

`layers_padding`:  Padding between the sections layers.

`sections_z_mul`: Sections multiplier among the Z axis.

`sections_back_wall`: Sections back wall thickness (in percentage).

Notes: the sections should take as much space as possible, but don't overlap with each other.

### 1.9 String mount config

`string_path_radius`: Radius of the string path.

`string_path_size_mul`: String path size multiplier.

`string_path_z_pos`: Position of the string path. Set to "auto" if the `sections_layers` is even.

`string_path_end_left_radius`: Left string path box angle \[3\].

`string_path_end_left_offset`: Left string path offset (pointing from the model).

`string_path_end_right_radius`: Right string path box angle \[4\].

`string_path_end_right_offset`: Right string path offset.

`string_path_end_size`: String path box size.

Note: the string path boxes are used for cutting the holes for the strings.

### 1.10 Checking the configuration

To check the configuration set the `mode` variable to `config` and hit F5.

![image](modeling3.png "FG-01 modeling")

### 1.11 Offset config

`offset_scale`: Shell scale in percentage.

`string_path_shell_scale`: String path shell scale in percentage.

`string_path_end_shell_scale`: String path box shell scale in percentage.

`sections_shell_scale`: Sections shell scale in percentage.

`sections_hide_offset`: Outer sections wall offset for hiding it under the lattice.

`sections_hide_offset_clearance`: Outer sections wall thickness.

`outer_shell_z`: Bottom shell height (lattice support).

Note: `sections_hide_offset` and `sections_hide_offset_clearance` affect the segments walls thickness. These values are set in percentage. Don't set much higher values than recommended. If you change these parameters, don't forget to render the models again.

### 1.12 Final check

OpenSCAD will attempt to render the final look. Note that the app may crash due to non-manifold mesh. Please save at this step. Change the `mode` variable to `subtract` and hit F5.

Pay attention to the string path: there should be cuts at the end.

### 1.13 Lattice config

`lattice_translate_x`, `lattice_translate_y`, `lattice_translate_z`: Lattice pattern position.

`lattice_scale`: Not recommended to change this variable (Deprecated).

`lattice_size`: The amount of lattice vertices. 12 is optimal, but you may set higher values. Low values are not recommended.

`lattice_bake_size`: The amount of baked lattice vertices, `lattice_size` must divide by this variable. 4 is optimal, 2 is minimal. Must be even.

`lattice_radius`: Lattice cell radius. Lower values are not recommended for 3D-printing.

`lattice_height_mul`: Lattice cell height. 4 is optimal.

`lattice_supports`: Generate lattice support (cell). Greatly increases the strength.

`lattice_fn`: Lattice render quality. 30 is optimal, higher is recommended for high-end PCs.

### 1.14 Lattice config check

Change the `mode` variable to `lattice_conf` and hit F5.

The box should cover the whole model. Leave some padding between the model and the box faces.

### 1.15 Rendering

Save the file.

Set `mode` to `bake_lattice` and press F6 (render). This may take a long time. Press F7 to export the STL. Save as `lattice_chunk.stl`.

Change `mode` to `render`. Press F5 and check the result. Press F6 to render and F7 to export `out.stl`.

Change `mode` to `render_boot` and press F6, F7. Save as `boot_out.stl`.

Set `mode` to `render_shell`. Press F5 and check if the model is fine. Press F6 and F7 to save the STL `shell_out.stl`.

Change mode to `render_segments`. Press F5 and make sure that the gray wall isn't too thin or too thick. Change the `sections_back_wall` value if needed. Press F6 and save the STL `segments.stl`.

You have now finished the model generation.

## 2. Blender

Since OpenSCAD needs absolutely clean STLs to work with (an aspect of CGAL polyhedron), so we're going to use Blender for actual render.

Open Blender, go to the Scripting layout and import the `process_model.py` script.

### 2.1 Configuration

`out_model_path`: Path to the `out.stl` file.

`shell_path`: Path to the `shell_out.stl` file.

`boot_path`: Path to the `boot_out.stl` file.

`lattice_chunk_path`: Path to the `lattice_chunk.stl` file.

`segments_path`: Path to the `segments.stl` file.

`segments_export_path`: Path to the segments output folder.

`offset`: Boot offset (model inner wall).

`scale_factor`: Must be equal to the `scale_factor` variable.

`lattice_count`: Amount of the lattice blocks.

`boolean_self`: Process self-intersections (affects performance).

`boolean_self_list`: Enable/disable self-intersections processing for each modifier.

`boolean_tbs`: Boolean modifier troubleshooting.

`boolean_tbs_list`: Enable/disable modifier troubleshooting for each modifier.

### 2.2 Processing

When you have finished the configuration, set the `render_segments` variable to `False` and hit the Run (arrow) button. The render may take a while.

Go to the Layout tab and check if the meshes are rendered correctly. If some objects are empty or there are artifacts, set the `boolean_self` variable to `True`. If some sections appear to be closed, change the third variable under `boolean_self_list` to `True`. Note that the compilation time will increase greatly. If other objects have artifacts or they aren't displayed, change the corresponding variable in `boolean_self_list` (The objects are listed in such order: `Shell Out`, `Out`, `Out` (modifier 2) and `Out.001`).

In case if some objects are still empty or you still see artifacts, you may want to enable the fast solver. Note that it doesn't process overlapping faces, and the resulting mesh may be completely broken. To enable this solver, please change `boolean_tbs` to `True`. If the `Out` object is empty, change the second variable under `boolean_tbs_list` to `True`. You can enable/disable it for other objects by changing the `boolean_tbs_list` variable (exactly like `boolean_self_list`).

Move and resize the lattice model so that it covers the `Out.001` model completely, but there is still little padding between the models. Select the `Out.001` model. Go to `Modifiers` tab (wrench icon), click Add Modifier -> Boolean. Select `Difference` and choose the `Lattice Chunk` model under `Object`. When the process is finished, click the down arrow and select `Apply`. This may take a long time. Please export all models, because Blender may crash during the final merge. Now select the `Out` model and add the boolean modifier (now select `Union` instead of `Difference`). Choose the resulting model as the object. Repeat the same steps and wait. Select the resulting model and click `File` -> `Export` -> `Stl`. Tick the `Selection Only` box and press `Export STL`. In case of a crash (or endless processing) you may try using FreeCAD instead. Open FreeCAD, go to the Mesh Design tab. Import `out` and `out.001` models, select them and click `Meshes` -> `Merge`. Right click on the model and export the STL.

Now you may press `A` -> `Delete`. Go to the `Scripting` tab and change the `render_segments` variable to `True`. Run the script again, the resulting segments shells will be written to the `segments_export_path`. In case if some segments are missing, set the `segments_self` variable to `True`. If any artifacts are still present, change `segments_tbs` to `True`. Congratulations, you have now finished the modeling.
