# \[TESTING\]

This part will help you to check if your configuration protects from basic gait recognition software. You shouldn't rely on its results, because actual deployed gait NNs are far more advanced.
		
Please read https://gitlab.com/FG-01/fg-01-tests tips to improve spoofing.

The gait NN scripts and documentation are located at https://gitlab.com/FG-01/fg-01-tests. 
