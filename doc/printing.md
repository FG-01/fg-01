# \[PRINTING\]

Even if you don't own a 3D printer, you can print out the model for a decent price. Ask your friends who own a 3D printer: this is by far the cheapest and the easiest method of getting a model. Also, there are services that allow 3D printer owners to find clients (like https://3dprint.alexgyver.ru/). The prices are usually very low. If there are none in your city, the only left option is a 3D printing service. Be prepared for high prices.

When you find your option, remember to duplicate and mirror your model, since you will need a pair of those.

You may also want to print clips for the rubber band. The model is placed in the `extra` folder. Check your rubber band width, you may manually change the FreeCAD project file. Don't forget to print two of them.

### 1. Material

Personally, I would recommend PETG, since it's chemically resistant and durable. PLA usually degrades with time, especially outdoors. ABS is a good option if you manage to print it.

### 2. Settings

You may set 0.25 - 0.35 layer height to speed up the print, but don't set the speed too high: lattice may not print out well. Leave supports only in sections, it won't be possible to remove them inside the lattice. Enable cooling. It's better to print out one model at a time, because something may go wrong during the print. If you have access to the printer during the whole time, you can install the rubber band in the middle of the print. You could add the pause directly to the GCODE file (it's the same trick that is used for filament swap).

### 3. Printing

Make sure that your build plate adhesion is fine: the model's first layer area is relatively small. If you're going to install the rubber band during the print, make sure that it wouldn't touch the moving parts and the nozzle.

### 4. Finishing

Carefully remove the support material. The inner surface and the sections walls may require sanding.
