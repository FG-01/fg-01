# \[FAQ\]

### NVIDIA GPU

> I don't have a NVIDIA GPU. What can I do?

If you prefer Meshroom, you could run it on Goole COLAB (https://github.com/paramphy/meshroom_colab). Alternatively, Regard3D doesn't require CUDA.

### OVER-ENGINEERED SOLUTION

> Why would you bother so much with such an easy task? I could achieve the same result with ...

Sure, I agree that FG-01 is an over-engineered solution, but attaching weight to the boot using duct tape would look suspicious. FG-01 attempts to look stylish (it was inspired by Adidas 3D-printed shoes).

### CLOTHES
> Since FG-01 should protect me from gait recognition, why do I need to wear different clothes?

Gait recognition detects your body shape and attempts to create a mathematical model. The silhouette includes your clothes, so it's important to change it when you don't want to be detected.

### HEALTH
> Why can't I wear FG-01 often? Which problems may occur in long-term?

https://reddit.com/u/The_Bush_Ranger posted a detailed comment on this topic:
> Tracker here. We are familiar with these types of strategies in an evasion context. They don't work very well against human trackers primarily because the negative health impacts on the user make them hard to use consistently. Defeating other types of (non human) tracking using these methods would have same problem. Changing "foot tilt angle" (the technical term for this is "pitch") is especially problematic as it leads to misalignment in the entire kinetic chain of the foot whose pitch was changed; it causes chronic pain in the knee and same side hip in a relatively short amount of time. Changing pitch also changes the strike (strike is where the foot first touches the ground), leading to blisters and an increased risk of ankle injury. Changing "step length" (the technical term is "stride" and in humans is specifically measured from toe to next heel, you don't define "step length" or state how you measure it) can also cause health problems because changing stride changes the angle at which the torso is held over the hips. These problems are well known in the tactical/combat tracking community, and when doing evasion exercises the quarry won't use these methods for more than a couple miles at most because of the health problems they cause. When I play the quarry I always schedule a sports massage and chiropractic adjustment for the day after because of the pain and inflammation caused by misalignment.

### SAFETY
> Is there any safe period of time to wear FG-01? I don't need any health problems.

Again, thanks https://reddit.com/u/The_Bush_Ranger for answering this question.
> It would probably be okay to wear for a few hours. You could wear it for days without serious long term injury but it would be painful to say the least and require PT. One thing to keep in mind is the difference in terrain. In tracking we typically trail our quarry over wilderness or rural terrain, in such terrain the strike/stride/straddle/pitch differ between every step because the terrain is not uniform. That means that in such terrain you can wear these kinds of things for relatively longer periods because the body isn't stressing the exact same area every step. In urban terrain the stress is extremely repetitive. Because the terrain is uniform the stress of walking is identical between steps, which means that instead of many small stressors spread throughout the kinetic chain you get few large stressors focused on small areas of the chain. I would expect that in urban terrain using this kind of boot insert would lead to health problems quicker in urban terrain. I personally wouldn't wear this product in urban terrain for the length of a protest although I would be comfortable wearing it to infiltrate/exfiltrate the operational area.

### BETTER PROTECTION
> Is there any guarantee that I won't be recognized? I may get in trouble if I end up in the city database.

No. FG-01 can only provide some basic protection from the gait recognition, but it may or may not protect from the more advanced algorithms. If you have a big threat model or you really don't want to be detected, you should pick a better alternative. Mascot suits offer superb protection from face, gait and body shape recognition. Furthermore, it may protect you from laser heartbeat and infrared heatmap recognition (Tests needed). Also, faux fur breaks proximity ultrasonic sensors. If you are willing to use it, make sure that your eyes and other body parts are hidden and there is enough padding to obfuscate your body shape. Obviously, it will limit your physical movements, this may be an issue when attending a protest. Regarding to the suspiciousness part, the suit may only draw positive attention or interest. You may be asked to show your face in some cases, though. It would be easy to cite your work, for example, whether someone asks the reason. Note that it's not a silver bullet, since it makes you pseudonymous, so you should avoid taking off the suit's parts under the surveillance and always plan your route.
