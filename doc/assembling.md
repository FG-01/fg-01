# \[ASSEMBLING\]

## 1. Weight

Choosing right weight material is crucial, because spoofing heavily relies on it.

### Be careful when choosing metals as a weight, because it may cause problems at public places with metal detectors. Be ready to give explanation.

We cannot give you any suggestion whether it's better to sacrifice security for better and more reliable spoofing. Take desicion according to your threat model and try avoiding places with metal detectors, if possible.

### 1.1 Light weight

Sculpting plasticine may be a decent variant, since it doesn't degrade over time and may be pretty dense (up to 1.6 - 1.8 g/cm^3). Always check its density before buying, because low density plasticine (1.0 - 1.6) hardly affects your gait. PLA/PETG filament has similar density and may be a siplier solution in such case.

Different types of clay may be also suitable as a weight, but it may lose its density over time due to desiccation.

Concrete is pretty dense (2.0+ g/cm^3), so it can also be used. You should 3d print sections and fill them with beton.

### 1.2 Medium weight

Any metal balls can be used as a weight, but you shouldn't expect consistent results. Use any plasticine to secure them in place.

### 1.3 Heavy weight

Lead fishing weight is by far the best solution due to low price, very high density and accessibility. Olive shape fits the best, but you can give it any shape (note that melting point of PLA is 60 degrees, PETG - 70 and ABS - 100). You can also get a set of tiny lead balls that will use all of the available space inside of a section. You could secure the weight in place with any plasticine.

## 2. Assembling

Firstly, you would need to pull the rubber band through the model. You could attach a small object to a string and roll it through the model. Then you should use the string to pull the band through the path. Also, you could use zipties: make a tiny hole in the rubber band, pull the ziptie through the hole and use this ziptie to pull the band. If the ziptie isn't long enough, attach zipties to each other.

Make sure that the model doesn't have sharp edges that could damage the boot. Sand them if needed.

You may want to cover the inner face of the model with black tape to cover the sections.

![image](assembling1.png "Assembling FG-01")

## 3. Testing

Try putting FG-01 on your boot. Don't apply too much force, if you cannot put it on, you should use hot water. Check your filament's melting point and use hot tap water to soften the model. Place it in the water for 1-1,5 minutes and quickly but carefully put it on the boot. The model will slightly deform to fit your boot.

