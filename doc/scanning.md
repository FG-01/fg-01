# \[SCANNING\]

## Introduction

In order to attach weight to the boot, we will need to create its 3D model. Photogrammetry is a free and relatively easy method for scanning an object. We will be covering Alicevision Meshroom (available for Linux and Windows).

## 1. Download

Go to https://alicevision.org/#meshroom
Compiling from source is overly complicated, because there are dozens of dependencies.

## 2. Capturing and processing

### 2.1 Requirements

* Any digital camera, a smartphone will be enough. DSLR will produce the best result
* Any printer (for CCTAG3)

### 2.2 Preparation

Print out CCTAG3 tags https://github.com/alicevision/CCTag

You may download the [A4 file](cctag3.png). Please don't separate the markers, it would be harder to measure the distance between them.

Alternatively, you could place a small object near the model and scale it to the real-world dimensions later. 

### 2.3 Capturing

![image](scanning1.png "FG-01 scanning")

Please read the official tutorial first: https://meshroom-manual.readthedocs.io/en/latest/tutorials/sketchfab/sketchfab.html

Find a suitable well lit place. Please follow these recommendations: https://meshroom-manual.readthedocs.io/en/latest/capturing/capturing.html

If your camera supports RAW format, use it. Always check the image exposure: too dark or too bright parts of the image won't process at all.

If the model is dark or it reflects light, you should cover the back of your model with painting tape: this will drastically increase the scanning precision.

### 2.4 Processing

Open Meshroom and load the captured images.

The guide is available here: https://github.com/alicevision/meshroom/wiki/CCTAG-scaling

Enable `CCTAG3` under Descriptor types in FeatureExtraction node. Insert the SfmtTransform node between StrucrtureFromMotion and PrepareDenseScene. Select `from_markers` as the Transformation Method. Disable everything except `CCTAG3` under LandmarksDescriberTypes.

Under Markers add 2 markers with IDs 0 and 1. Measure the distance between the centers of printed markers. Put the measured distance under the x coordinate of the Marker 1 in decimeters (if the final model is too small, scale it up by 100).

Now you may start the render.

### 2.5 Cleaning

Open the generated mesh in Meshlab.

Use `Edit` -> `Select Faces in a Rectangular Region` to remove floating faces around the model. Save the file at this point, since Meshlab may crash later.

Select `Filters` -> `Cleaning and repairing` -> `Remove T-Vertices by Edge flip`. If the program crashes, leave the model as-is.

Open this mesh in Blender. Add `Mesh` -> `Cube` and place it under the model. Select the `Scale` tool and stretch it to fit the model. The cube should cover up the bottom hole and be sligtly higher than the model's base (floor).

Click on the boot model, select `Add modifier` -> `Boolean` and perform the union operation between these objects. If the hole doesn't cover up or you see artifacts, choose intersect instead. If this doesn't help, select the cube, go to the `Edit` mode and subdivide the plane multiple times (Right click -> `Subdivide`).

Go into Edit mode, choose `Select` -> `None`, `Select` -> `All by Trait` -> `Non Manifold` and press Delete. Please check if there are holes in the model, in this case undo the prevoius operation.

Make sure that they are no vertices hanging in the air. You can delete them by selecting and pressing Del (this may take a long time, use the last method instead).

If the back of the model has dirty vertices, you may need to clean them using sculpting. Use the Draw brush to fix the holes and pointing out vertices. Use Smooth brush with care, because it changes the model's topology. 

Another way to clean the model is to manually patch all the holes in the model. Firstly, add a sphere and scale it so it fully covers the inner part of the boot. Make sure that it's fully inside of the model, but there are no "air pockets". You could inspect the model by changing to `Wireframe` viewport shading. When you're done, select the boot model, add `Boolean` modifier, select `Union` and choose the sphere as the target. Apply the modifier and go to the `Edit` mode. Select `Mesh` -> `Separate` -> `By Loose Parts`. Delete all of the resulting objects, excluding the base model.

Export the model as STL.

##### Additional resources:

Please read these tutorials, they will help you to understand this instructions much better. 

* Verbose photogrammetry tutorial https://youtu.be/1D0EhSi-vvc
* Official AliceVision tutorial https://meshroom-manual.readthedocs.io/en/latest/tutorials/sketchfab/sketchfab.html
* Official CCTAG-scaling tutorial https://github.com/alicevision/meshroom/wiki/CCTAG-scaling
