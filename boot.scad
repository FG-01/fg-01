include <hex.scad>;
$fn=70;

// Model generation parameters
angle=5;
radius=25;

// Model scale
scale_x=1;
scale_y=0.8;
scale_z=1;

// Model scaling factor
scale_factor=1;

// 3D scan position
translate_x=0.05;
translate_y=0.13;
translate_z=-0.025;

// 3D scan rotation
scan_rotate_x=0;
scan_rotate_y=0;
scan_rotate_z=0;

// 3D scan path
scan_stl_path="scan/scan.stl";

// Sections config
sections_count=16; // 360/sections_count
sections_size=5;
sections_angle=angle;
start_angle=40;
end_angle=320;
sections_layers=2;
layers_offset_z=-1*scale_factor;
layers_padding=10*scale_factor;
sections_z_mul=1.2;
sections_back_wall=0.02; // Sections back wall thickness

// String mount config
string_path_radius=2;
string_path_size_mul=1.15;
string_path_z_pos="auto"; // "auto" if sections_layers is even
string_path_end_left_radius=330;
string_path_end_left_offset=0;
string_path_end_right_radius=30;
string_path_end_right_offset=0;
string_path_end_size=10;

// Offset config
offset_scale=0.05; // Shell scale in percentage
string_path_shell_scale=0.25; // String path shell scale percentage
string_path_end_shell_scale=0.2;
sections_shell_scale=0.15;
sections_hide_offset=0.1; // Outer sections wall offset
sections_hide_offset_clearance=0.01; // Prevent overlapping faces
outer_shell_z=-10; // Shell bottom support height

// Lattice config
lattice_translate_x=-44;
lattice_translate_y=-38;
lattice_translate_z=-20;
lattice_scale = 1; // Higher -> smaller
lattice_size=12;
lattice_bake_size=4; // lattice_size should divide by lattice_bake_size
lattice_radius=1.2;
lattice_height_mul=4;
lattice_supports=true;
lattice_fn=30;
// Post-bake parameters (Deprecated)
// lattice_final_mul=2;
// lattice_final_scale=0.7;

// ------MENU------

// Scroll down to the mode declaration for instructions
mode="position"; // "position", "check_1", "config", "subtract", "lattice_conf", "bake_lattice", "render", "render_boot", "render_shell", "render_segments", "render_segments_form", "devel"

// ------END-------

module triangle(){
    mirror([1, 0, 0])
    union(){
        translate([5, 0, 0])
        resize([25, 15])
        circle(10);
        translate([1, 7, 0])
        rotate([0, 0, -30])
        resize([15, 25])
        circle(10);
        translate([9, 7, 0])
        rotate([0, 0, 30])
        resize([15, 25])
        circle(10);
    }
}

module main_shape(){
    scale(scale_factor)
    scale([scale_x, scale_y, scale_z])
    rotate([0, angle, 0])
    rotate_extrude(convexity = 10)
    translate([radius, 0, 0])
    triangle();
}

module main_shape_sm(scaling){
    scale(scale_factor)
    scale([scale_x, scale_y, scale_z])
    rotate([0, angle, 0])
    rotate_extrude(convexity = 10)
    translate([radius, 0, 0])
    scale([scaling, scaling, scaling])
    triangle();
}

module position(check){
    echo("--------> Starting positioning...");
    translate([translate_x, translate_y, translate_z])
    rotate([scan_rotate_x, scan_rotate_y, scan_rotate_z])
    import(scan_stl_path);

    if(check) {
        #main_shape();
    } else {
        main_shape();
    }
}

module segments(s_scale=1){
    rotate([0, sections_angle, 0])
    for (i=[start_angle:360/sections_count:end_angle]) {
        rotate([0, 0, i])
        translate([(radius*scale_factor)/2, 0])
        translate([5*scale_factor, 0, 0])
        scale(scale_factor)
        scale([10, 1, sections_z_mul])
        cube(sections_size*s_scale, center=true);
    }
}

module segments_layers(s_scale=1){
    for (i=[1:1:sections_layers]) {
        translate([0, 0, layers_offset_z+(i-1)*layers_padding])
        segments(s_scale);
    }
}

module string_thread(z_pos, rad){
    translate([0, 0, z_pos])
    rotate([0, sections_angle, 0])
    scale([scale_x, scale_y, scale_z])
    rotate_extrude(convexity=10)
    scale(scale_factor)
    translate([radius*string_path_size_mul, 0, 0])
    circle(rad);
}

module string_end(deg, z_pos, off, s_size){
    scale([scale_x, scale_y, scale_z])
    translate([0, 0, z_pos])
    rotate([0, 0, deg])
    scale(scale_factor)
    translate([radius*string_path_size_mul+off, 0, 0])
    cube(s_size, center=true);
}

module gen_string_thread(){
    if (string_path_z_pos == "auto") {
        z_pos=layers_offset_z+((sections_layers-1)/2)*layers_padding;
        string_thread(z_pos, string_path_radius);
        rotate([0, sections_angle, 0])
        string_end(string_path_end_left_radius, z_pos, string_path_end_left_offset, string_path_end_size);
        rotate([0, sections_angle, 0])
        string_end(string_path_end_right_radius, z_pos, string_path_end_right_offset, string_path_end_size);
    } else {
        string_thread(string_path_z_pos, string_path_radius);
        rotate([0, sections_angle, 0])
        string_end(string_path_end_left_radius, string_path_z_pos, string_path_end_left_offset, string_path_end_size);
        rotate([0, sections_angle, 0])
        string_end(string_path_end_right_radius, string_path_z_pos, string_path_end_right_offset, string_path_end_size);
    }
}

module gen_string_thread_shell(){
    if (string_path_z_pos == "auto") {
        z_pos=layers_offset_z+((sections_layers-1)/2)*layers_padding;
        string_thread(z_pos, string_path_radius*(1+string_path_shell_scale));
        rotate([0, sections_angle, 0])
        string_end(string_path_end_left_radius, z_pos, string_path_end_left_offset, string_path_end_size*(1+string_path_end_shell_scale));
        rotate([0, sections_angle, 0])
        string_end(string_path_end_right_radius, z_pos, string_path_end_right_offset, string_path_end_size*(1+string_path_end_shell_scale));
    } else {
        string_thread(string_path_z_pos, string_path_radius*(1+string_path_shell_scale));
        rotate([0, sections_angle, 0])
        string_end(string_path_end_left_radius, string_path_z_pos, string_path_end_left_offset, string_path_end_size*(1+string_path_end_shell_scale));
        rotate([0, sections_angle, 0])
        string_end(string_path_end_right_radius, string_path_z_pos, string_path_end_right_offset, string_path_end_size*(1+string_path_end_shell_scale));
    }
}

module lattice_box(){
    scale(scale_factor)
    translate([lattice_translate_x, lattice_translate_y, lattice_translate_z])
    draw_box(lattice_size, lattice_height_mul);
}

module gen_lattice_pattern(){
    scale(scale_factor)
    translate([lattice_translate_x, lattice_translate_y, lattice_translate_z])
    scale(1/lattice_scale)
    gen_lattice(lattice_size, lattice_radius, lattice_height_mul, lattice_supports, lattice_fn);
}

module lattice_chunk(){
    difference(){
        //scale(scale_factor)
        //translate([lattice_translate_x, lattice_translate_y, lattice_translate_z])
        //scale(1/lattice_scale)
        draw_box(lattice_bake_size, lattice_height_mul);
        
        //scale(scale_factor)
        //translate([lattice_translate_x, lattice_translate_y, lattice_translate_z])
        //scale(1/lattice_scale)
        gen_lattice(lattice_bake_size, lattice_radius, lattice_height_mul, lattice_supports, lattice_fn);
    }
}

module bake_lattice(){
    // Deprecated
    // Doesn't produce manifold stl
    scale(scale_factor)
    translate([lattice_translate_x, lattice_translate_y, lattice_translate_z])
    union() {
        for(i=[1:1:lattice_size/lattice_bake_size]) {
            for(j=[1:1:lattice_size/lattice_bake_size]) {
                for(l=[1:1:lattice_size/lattice_bake_size]) {
                    translate([(i-1)*height_mul*(lattice_bake_size), (j-1)*height_mul*(lattice_bake_size), (l-1)*height_mul*(lattice_bake_size)])
                    //lattice_chunk();
                    import("tmp/lattice_chunk.stl");
                }
            }
        }
    }
}

module bake_lattice_final(){
    // Deprecated
    union() {
        for(i=[1:1:lattice_final_mul]) {
            for(j=[1:1:lattice_final_mul]) {
                for(l=[1:1:lattice_final_mul]) {
                    translate([(i-1)*height_mul*(lattice_size/lattice_bake_size+1)*lattice_bake_size*scale_factor, (j-1)*height_mul*(lattice_size/lattice_bake_size+1)*lattice_bake_size*scale_factor, (l-1)*height_mul*(lattice_size/lattice_bake_size+1)*lattice_bake_size*scale_factor])
                    //lattice_chunk();
                    import("tmp/bake_lattice.stl");
                }
            }
        }
    }
}

module model_diff(){
    difference(){
        main_shape();
        translate([translate_x, translate_y, translate_z])
        rotate([scan_rotate_x, scan_rotate_y, scan_rotate_z])
        import(scan_stl_path);
    }
}

// ---------MODES---------
if(mode == "position") {
    // Adjust the scale and the position of the model
    position(false);
}

if (mode == "check_1") {
    // Check if the model fits well
    position(true);
}

if (mode == "config") {
    // Place the weight sections
    #difference(){
        main_shape();
        segments_layers();
    }
    gen_string_thread();
}

if (mode == "subtract") {
    // Subtracting the scan from the model
    difference() {
        model_diff();
        intersection() {
            scale([1-offset_scale, 1-offset_scale, 1-offset_scale])
            model_diff();
            segments_layers();
        }
        gen_string_thread();
    }
}

if (mode == "lattice_conf") {
    // Configure lattice size
    difference(){
        main_shape();
        segments_layers();
    }
    %lattice_box();
}

if (mode == "render") {
    // Render the model
    
    $fn=100; // Comment out if render takes a long time
    
    /*intersection(){
        difference(){
            main_shape();
            segments_layers();
        }
        gen_lattice_pattern();
    }*/
    //gen_lattice_pattern();
    difference() {
        main_shape();
        intersection() {
            scale([1-sections_hide_offset-sections_hide_offset_clearance, 1-sections_hide_offset-sections_hide_offset_clearance, 1-sections_hide_offset-sections_hide_offset_clearance])
            main_shape();
            segments_layers();
        }
        gen_string_thread();
    }
}

if (mode == "bake_lattice") {
    // Generate lattice chunk for further pattern genereation
    lattice_chunk();
    //bake_lattice();
    //gen_lattice_pattern();
}

if (mode == "render_boot") {
    // Render the boot
    translate([translate_x, translate_y, translate_z])
    rotate([scan_rotate_x, scan_rotate_y, scan_rotate_z])
    import(scan_stl_path);
}

if (mode == "render_shell") {
    // Render the lattice shell
    
    $fn=100; // Comment out if render takes a long time
    difference() {
        union() {
            intersection() {
                main_shape();
                scale([1-offset_scale, 1-offset_scale, 1-offset_scale])
                main_shape();
            }
            difference() {
                scale([1+offset_scale, 1+offset_scale, 1+offset_scale])
                main_shape();
                rotate([0, angle, 0])
    translate([0, 0, outer_shell_z*scale_factor])
                scale([1+radius*10*scale_factor, 1+radius*10*scale_factor, 1])
                cube(radius*scale_factor, center=true);
            }
        }
        gen_string_thread_shell();
        intersection() {
            scale([1-sections_hide_offset, 1-sections_hide_offset, 1-sections_hide_offset])
            main_shape();
            segments_layers(1+sections_shell_scale);
        }
    }
}

if (mode == "render_segments") {
    // Render segments model

    union() {
        intersection() {
            scale([1-sections_hide_offset-sections_hide_offset_clearance, 1-sections_hide_offset-sections_hide_offset_clearance, 1-sections_hide_offset-sections_hide_offset_clearance])
            main_shape();
            difference() {
                segments_layers();
                segments_layers(1-sections_shell_scale);
            }
        }
        intersection() {
            difference() {
                scale([1-sections_hide_offset-sections_hide_offset_clearance, 1-sections_hide_offset-sections_hide_offset_clearance, 1-sections_hide_offset-sections_hide_offset_clearance])
                main_shape();
                scale([1-sections_hide_offset-sections_hide_offset_clearance-sections_back_wall, 1-sections_hide_offset-sections_hide_offset_clearance-sections_back_wall, 1-sections_hide_offset-sections_hide_offset_clearance-sections_back_wall])
                main_shape();
            }
            segments_layers();
        }
    }
    %intersection() {
        difference() {
            scale([1-sections_hide_offset-sections_hide_offset_clearance, 1-sections_hide_offset-sections_hide_offset_clearance, 1-sections_hide_offset-sections_hide_offset_clearance])
            main_shape();
            scale([1-sections_hide_offset-sections_hide_offset_clearance-sections_back_wall, 1-sections_hide_offset-sections_hide_offset_clearance-sections_back_wall, 1-sections_hide_offset-sections_hide_offset_clearance-sections_back_wall])
            main_shape();
        }
        segments_layers(1+sections_shell_scale);
    }
}

if (mode == "render_segments_form") {
    // Deprecated
    // Render non-hollow segments

    intersection() {
        scale([1-sections_hide_offset-sections_hide_offset_clearance, 1-sections_hide_offset-sections_hide_offset_clearance, 1-sections_hide_offset-sections_hide_offset_clearance])
        main_shape();
        difference() {
            segments_layers();
        }
    }
}

if (mode == "devel") {
    // Debugging function

    intersection() {
        scale([1-sections_hide_offset-sections_hide_offset_clearance-sections_back_wall, 1-sections_hide_offset-sections_hide_offset_clearance-sections_back_wall, 1-sections_hide_offset-sections_hide_offset_clearance-sections_back_wall])
        main_shape();
        segments_layers(1-sections_shell_scale);
    }
}
